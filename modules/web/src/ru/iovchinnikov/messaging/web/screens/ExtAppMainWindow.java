package ru.iovchinnikov.messaging.web.screens;

import com.haulmont.cuba.gui.WindowManager;
import com.haulmont.cuba.web.app.mainwindow.AppMainWindow;

public class ExtAppMainWindow extends AppMainWindow {

    public void messagesBrowse() {
        openWindow("message-with-tabs", WindowManager.OpenType.DIALOG);
    }
}