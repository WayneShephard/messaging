package ru.iovchinnikov.messaging.web.message;

import com.haulmont.cuba.gui.components.*;
import com.haulmont.cuba.gui.components.actions.CreateAction;
import com.haulmont.cuba.gui.components.actions.EditAction;
import com.haulmont.cuba.security.global.UserSession;
import ru.iovchinnikov.messaging.entity.Message;
import ru.iovchinnikov.messaging.service.MessageService;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Map;
import java.util.Set;

public class MessageWithTabs extends AbstractWindow {

    private static final String TAB_SENT = "tabSent";
    private static final String TAB_INBOX = "tabInbox";
    private static final String TAB_RECYCLE = "tabRecycle";
    private static final String TABLE_SENT = "sentTable";
    private static final String TABLE_SENT_CREATE = "sentTable.create";
    private static final String TABLE_SENT_EDIT = "sentTable.edit";
    private static final String TABLE_INBOX = "inboxTable";
    private static final String TABLE_INBOX_CREATE = "inboxTable.create";
    private static final String TABLE_INBOX_EDIT = "inboxTable.edit";
    private static final String TABLE_RECYCLE = "recycleTable";
    private static final String TABLE_RECYCLE_CREATE = "recycleTable.create";
    private static final String TABLE_RECYCLE_EDIT = "recycleTable.edit";

    private static final String MESSAGE_VIEW_WINDOW = "messaging$Message.view";

    @Named("allTabs")
    private TabSheet tabSheet;

    @Named(TABLE_INBOX_CREATE) private CreateAction createInbox;
    @Named(TABLE_INBOX_EDIT) private EditAction editInbox;
    @Named(TABLE_SENT_CREATE) private CreateAction createSent;
    @Named(TABLE_SENT_EDIT) private EditAction editSent;
    @Named(TABLE_RECYCLE_CREATE) private CreateAction createRecycle;
    @Named(TABLE_RECYCLE_EDIT) private EditAction editRecycle;

    @Named(TABLE_INBOX) private GroupTable<Message> inboxTable;
    @Named(TABLE_SENT) private GroupTable<Message> sentTable;
    @Named(TABLE_RECYCLE) private GroupTable<Message> recycleTable;

    @Inject
    private UserSession userSession;

    @Inject
    private MessageService messageService;

    private void screenRefresh(String id) {
        inboxTable.getDatasource().refresh();
        sentTable.getDatasource().refresh();
        recycleTable.getDatasource().refresh();
    }

    @Override
    public void init(Map<String, Object> params) {
        editInbox.setWindowId(MESSAGE_VIEW_WINDOW);
        editSent.setWindowId(MESSAGE_VIEW_WINDOW);
        editRecycle.setWindowId(MESSAGE_VIEW_WINDOW);

        createInbox.setEditorCloseListener(this::screenRefresh);
        editInbox.setEditorCloseListener(this::screenRefresh);
        createSent.setEditorCloseListener(this::screenRefresh);
        editSent.setEditorCloseListener(this::screenRefresh);
        createRecycle.setEditorCloseListener(this::screenRefresh);
        editRecycle.setEditorCloseListener(this::screenRefresh);
    }

    private void doRecycle(String source, Table<Message> table) {
        Set<Message> selected = table.getSelected();
        for (Message m : selected) {
            messageService.recycle(source, m);
        }
    }

    private void doDelete(Table<Message> table) {
        String currentLogin = userSession.getUser().getLogin();
        Set<Message> selected = table.getSelected();
        for (Message m : selected) {
            if (m.getSender().getLogin().equals(currentLogin))
                messageService.delete(MessageService.TX, m);
            else if (m.getReceiver().getLogin().equals(currentLogin))
                messageService.delete(MessageService.RX, m);
            else
                throw new RuntimeException("Current user is not sender or receiver: " + currentLogin);
        }
        screenRefresh("SUBSET");
    }

    public void recycle() {
        String source = tabSheet.getTab().getName();
        switch (source) {
            case TAB_SENT:
                doRecycle(MessageService.TX, sentTable);
                break;
            case TAB_INBOX:
                doRecycle(MessageService.RX, inboxTable);
                break;
            case TAB_RECYCLE:
                doDelete(recycleTable);
                break;
            default:
                throw new RuntimeException("Unknown tab name: " + source);
        }
        screenRefresh("RECYCLE");
    }

    public void recover() {
        String currentLogin = userSession.getUser().getLogin();
        Set<Message> selected = recycleTable.getSelected();
        for (Message m : selected) {
            if (m.getSender().getLogin().equals(currentLogin))
                messageService.recover(MessageService.TX, m);
            else if (m.getReceiver().getLogin().equals(currentLogin))
                messageService.recover(MessageService.RX, m);
            else
                throw new RuntimeException("Current user is not sender or receiver: " + currentLogin);
        }
        screenRefresh("RECOVER");
    }
}