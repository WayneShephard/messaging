package ru.iovchinnikov.messaging.web.message;

import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.core.global.Metadata;
import com.haulmont.cuba.gui.WindowManager;
import com.haulmont.cuba.gui.components.*;
import com.haulmont.cuba.security.global.UserSession;
import ru.iovchinnikov.messaging.entity.Message;
import ru.iovchinnikov.messaging.entity.MessageText;
import ru.iovchinnikov.messaging.service.MessageService;

import javax.inject.Inject;

public class MessageView extends AbstractEditor<Message> {

    private Message currentMsg;
    private boolean isUserReceiver;

    @Inject private UserSession userSession;
    @Inject private MessageService messageService;
    @Inject private Metadata metadata;
    @Inject private DataManager dataManager;

    @Override
    public void ready() {
        currentMsg = getItem();
        if ("".equals(currentMsg.getReceiverShow()))
            currentMsg.setReceiverShow(currentMsg.getReceiver().getName());
        if ("".equals(currentMsg.getSenderShow()))
            currentMsg.setSenderShow(currentMsg.getSender().getName());
        isUserReceiver = isUserReceiver();
        if (isUserReceiver) {
            currentMsg.getMeta().setIsRead(true);
        }
    }

    private boolean isUserReceiver() {
        return currentMsg.getReceiver().getLogin().equals(userSession.getUser().getLogin());
    }

    public void onBtnReplyClick() {
        //TODO не показывать кнопку ответа, если открыто отправленное сообщение
        if (!isUserReceiver) {
            showNotification(getMessage("msgCantReplySelf"));
            return;
        }
        this.commitAndClose();
        Message newMsg = metadata.create(Message.class);
        MessageText messageText = metadata.create(MessageText.class);
        messageText.setText("<div></div><div>________</div><div>" + currentMsg.getSender().getName() +
                " wrote: </div><div>" + currentMsg.getMessageText().getText() + "</div>");
        newMsg.setMessageText(messageText);
        newMsg.setSender(userSession.getUser());
        newMsg.setReceiver(currentMsg.getSender());
        newMsg.setSubject("Re: " + currentMsg.getSubject());
        openEditor(newMsg, WindowManager.OpenType.DIALOG);
    }

    public void onBtnRemoveClick() {
        if (isUserReceiver)
            messageService.recycle(MessageService.RX, currentMsg);
        else if (!isUserReceiver)
            messageService.recycle(MessageService.TX, currentMsg);
        else
            throw new RuntimeException("Unexpected behavior");
        this.close("OK");
    }

    public void onBtnReportClick() {
        String strSystem = getMessage("system");
        //TODO не отображать кнопку, если я отправитель, или если сообщение системное
        if (!isUserReceiver) {
            showNotification(getMessage("msgCantReportSelf"));
            return;
        }
        //TODO корректно локализовать строку "Система"
        // чтобы верно обрабатывались и русские и нерусские локализации
        if (currentMsg.getSenderShow().equals(strSystem)) {
            showNotification(getMessage("msgCantReportSystem"));
            return;
        }
        String sender = currentMsg.getSender().getLogin();
        String receiver = currentMsg.getReceiver().getLogin();
        String text = String.format(getMessage("msgTxtReport"), receiver, sender);
        messageService.send(strSystem, "admin", getMessage("msgSpamReport"), text);
        if (isUserReceiver)
            currentMsg.getMeta().setIsSenderRec(true);
        else if (!isUserReceiver)
            currentMsg.getMeta().setIsReceiverRec(true);
        else
            throw new RuntimeException("Unexpected behavior");
        dataManager.commit(currentMsg.getMeta());
        this.commitAndClose();
        // TODO объект Message.Meta был изменён в другой транзакции
    }

    public void onBtnFwdClick() {
        //TODO не показывать кнопку ответа, если открыто отправленное сообщение
        this.commitAndClose();
        Message newMsg = metadata.create(Message.class);
        MessageText messageText = metadata.create(MessageText.class);
        messageText.setText("<div></div><div>________</div><div>" + currentMsg.getSender().getName() +
                " wrote: </div><div>" + currentMsg.getMessageText().getText() + "</div>");
        newMsg.setMessageText(messageText);
        newMsg.setSender(userSession.getUser());
        newMsg.setSubject("Fw: " + currentMsg.getSubject());
        openEditor(newMsg, WindowManager.OpenType.DIALOG);
    }

    public void onBtnFavClick() {
        //TODO view archive/favorites method
    }
}