package ru.iovchinnikov.messaging.web.message;

import com.haulmont.cuba.core.global.Metadata;
import com.haulmont.cuba.gui.components.*;
import com.haulmont.cuba.security.global.UserSession;
import ru.iovchinnikov.messaging.entity.Message;
import ru.iovchinnikov.messaging.entity.MessageMeta;
import ru.iovchinnikov.messaging.entity.MessageText;

import javax.inject.Inject;
import javax.inject.Named;

public class MessageEdit extends AbstractEditor<Message> {

    @Inject private Metadata metadata;
    @Inject private UserSession userSession;
    @Inject private FieldGroup fieldGroup;
    @Inject private RichTextArea messageTextTextArea;

    @Override
    protected void initNewItem(Message item) {
        if (item.getMessageText() == null)
            item.setMessageText(metadata.create(MessageText.class));
        if (item.getMeta() == null)
            item.setMeta(metadata.create(MessageMeta.class));
        item.setSender(userSession.getUser());
        item.setSenderShow(userSession.getUser().getName());

        fieldGroup.setEditable(true);
        messageTextTextArea.setEditable(true);
    }

    @Override
    protected boolean preCommit() {
        getItem().setReceiverShow(getItem().getReceiver().getName());
        return super.preCommit();
    }
}