package ru.iovchinnikov.messaging.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;
import com.haulmont.cuba.core.entity.StandardEntity;

@Table(name = "MESSAGING_MESSAGE_TEXT")
@Entity(name = "messaging$MessageText")
public class MessageText extends StandardEntity {
    private static final long serialVersionUID = -4453960860495457920L;

    @Lob
    @Column(name = "TEXT", nullable = false)
    protected String text;

    public void setText(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

}