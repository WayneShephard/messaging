package ru.iovchinnikov.messaging.entity;

import javax.annotation.PostConstruct;
import javax.persistence.*;

import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.StandardEntity;
import com.haulmont.cuba.core.global.AppBeans;
import com.haulmont.cuba.core.global.Metadata;
import com.haulmont.cuba.security.entity.User;
import com.haulmont.cuba.core.entity.annotation.OnDelete;
import com.haulmont.cuba.core.global.DeletePolicy;
import javax.validation.constraints.NotNull;
import com.haulmont.chile.core.annotations.Composition;

@NamePattern("%s: |sender,subject")
@Table(name = "MESSAGING_MESSAGE")
@Entity(name = "messaging$Message")
public class Message extends StandardEntity {
    private static final long serialVersionUID = 1079735625753425111L;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SENDER_ID")
    protected User sender;

    @Column(name = "SENDER_SHOW")
    protected String senderShow;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "RECEIVER_ID")
    protected User receiver;

    @Column(name = "RECEIVER_SHOW")
    protected String receiverShow;

    @Column(name = "SUBJECT")
    protected String subject;

    @Column(name = "ENTITY_REFERENCE")
    protected String entityReference;

    @Composition
    @OnDelete(DeletePolicy.CASCADE)
    @OneToOne(fetch = FetchType.LAZY, optional = false, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "MESSAGE_TEXT_ID")
    protected MessageText messageText;

    @Composition
    @OnDelete(DeletePolicy.CASCADE)
    @OneToOne(fetch = FetchType.LAZY, optional = false, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "META_ID")
    protected MessageMeta meta;

    public void setSenderShow(String senderShow) {
        this.senderShow = senderShow;
    }

    public String getSenderShow() {
        return senderShow;
    }

    public void setReceiverShow(String receiverShow) {
        this.receiverShow = receiverShow;
    }

    public String getReceiverShow() {
        return receiverShow;
    }

    public void setMeta(MessageMeta meta) {
        this.meta = meta;
    }

    public MessageMeta getMeta() {
        return meta;
    }

    public void setMessageText(MessageText messageText) {
        this.messageText = messageText;
    }

    public MessageText getMessageText() {
        return messageText;
    }

    public void setEntityReference(String entityReference) {
        this.entityReference = entityReference;
    }

    public String getEntityReference() {
        return entityReference;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getSubject() {
        return subject;
    }

    public void setSender(User sender) {
        this.sender = sender;
    }

    public User getSender() {
        return sender;
    }

    public void setReceiver(User receiver) {
        this.receiver = receiver;
    }

    public User getReceiver() {
        return receiver;
    }

}