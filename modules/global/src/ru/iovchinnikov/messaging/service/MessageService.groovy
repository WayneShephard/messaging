package ru.iovchinnikov.messaging.service

import ru.iovchinnikov.messaging.entity.Message

interface MessageService {
    String NAME = "messaging_MessageService"
    public static final String RX = "Receiver"
    public static final String TX = "Sender"


    void send(String sender, String receiver, String subject, String text)

    void delete(String source, Message msg)

    void favorite(String source, Message msg)

    void recycle(String source, Message msg)

    void recover(String source, Message msg)
}