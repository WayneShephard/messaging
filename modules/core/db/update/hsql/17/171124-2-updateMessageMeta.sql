alter table MESSAGING_MESSAGE_META add column IS_SENDER_REC boolean ^
update MESSAGING_MESSAGE_META set IS_SENDER_REC = false where IS_SENDER_REC is null ;
alter table MESSAGING_MESSAGE_META alter column IS_SENDER_REC set not null ;
alter table MESSAGING_MESSAGE_META add column IS_RECEIVER_REC boolean ^
update MESSAGING_MESSAGE_META set IS_RECEIVER_REC = false where IS_RECEIVER_REC is null ;
alter table MESSAGING_MESSAGE_META alter column IS_RECEIVER_REC set not null ;
