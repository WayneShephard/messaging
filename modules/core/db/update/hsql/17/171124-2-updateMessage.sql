alter table MESSAGING_MESSAGE add column META_ID varchar(36) ;
alter table MESSAGING_MESSAGE drop column IS_DELETED_BY_SENDER cascade ;
alter table MESSAGING_MESSAGE drop column IS_DELETED_BY_RECEIVER cascade ;
alter table MESSAGING_MESSAGE drop column IS_READ cascade ;
