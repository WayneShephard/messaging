-- update MESSAGING_MESSAGE set RECEIVER_ID = <default_value> where RECEIVER_ID is null ;
alter table MESSAGING_MESSAGE alter column RECEIVER_ID set not null ;
