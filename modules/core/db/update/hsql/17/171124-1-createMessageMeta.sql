create table MESSAGING_MESSAGE_META (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    IS_READ boolean not null,
    IS_SENDER_DEL boolean not null,
    IS_RECEIVER_DEL boolean not null,
    IS_SENDER_FAV boolean not null,
    IS_RECEIVER_FAV boolean not null,
    --
    primary key (ID)
);
