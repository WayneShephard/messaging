create table MESSAGING_MESSAGE (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    SENDER_ID varchar(36) not null,
    IS_DELETED_BY_SENDER boolean,
    IS_DELETED_BY_RECEIVER boolean,
    RECEIVER_ID varchar(36),
    SUBJECT varchar(255),
    ENTITY_REFERENCE varchar(255),
    IS_READ boolean not null,
    MESSAGE_TEXT_ID varchar(36) not null,
    --
    primary key (ID)
);
