-- begin MESSAGING_MESSAGE
create table MESSAGING_MESSAGE (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    SENDER_ID varchar(36),
    SENDER_SHOW varchar(255),
    RECEIVER_ID varchar(36) not null,
    RECEIVER_SHOW varchar(255),
    SUBJECT varchar(255),
    ENTITY_REFERENCE varchar(255),
    MESSAGE_TEXT_ID varchar(36) not null,
    META_ID varchar(36) not null,
    --
    primary key (ID)
)^
-- end MESSAGING_MESSAGE
-- begin MESSAGING_MESSAGE_TEXT
create table MESSAGING_MESSAGE_TEXT (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    TEXT longvarchar not null,
    --
    primary key (ID)
)^
-- end MESSAGING_MESSAGE_TEXT
-- begin MESSAGING_MESSAGE_META
create table MESSAGING_MESSAGE_META (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    IS_READ boolean not null,
    IS_SENDER_REC boolean not null,
    IS_RECEIVER_REC boolean not null,
    IS_SENDER_DEL boolean not null,
    IS_RECEIVER_DEL boolean not null,
    IS_SENDER_FAV boolean not null,
    IS_RECEIVER_FAV boolean not null,
    --
    primary key (ID)
)^
-- end MESSAGING_MESSAGE_META
