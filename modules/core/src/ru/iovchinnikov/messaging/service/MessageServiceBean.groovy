package ru.iovchinnikov.messaging.service

import com.haulmont.cuba.core.Persistence
import com.haulmont.cuba.core.global.DataManager
import com.haulmont.cuba.core.global.LoadContext
import com.haulmont.cuba.core.global.Metadata
import com.haulmont.cuba.security.entity.User
import org.springframework.stereotype.Service
import ru.iovchinnikov.messaging.entity.Message
import ru.iovchinnikov.messaging.entity.MessageMeta
import ru.iovchinnikov.messaging.entity.MessageText

import javax.inject.Inject

@Service(MessageService.NAME)
class MessageServiceBean implements MessageService {

    @Inject Persistence persistence
    @Inject DataManager dataManager
    @Inject Metadata metadata

    @Override
    void send(String sender, String receiver, String subject, String text) {
        Message newMsg = metadata.create(Message.class)
        newMsg.setMessageText(metadata.create(MessageText.class))
        newMsg.setMeta(metadata.create(MessageMeta.class))
        LoadContext loadContext = LoadContext.create(User.class)
                .setQuery(LoadContext.createQuery('select p from sec$User p where p.login = :username')
                .setParameter("username", receiver))
        User receiverUser = dataManager.load(loadContext)

        loadContext = LoadContext.create(User.class)
                .setQuery(LoadContext.createQuery('select p from sec$User p where p.login = :username')
                .setParameter("username", sender))
        User senderUser = dataManager.load(loadContext)

        if (senderUser != null) newMsg.sender = senderUser
        newMsg.senderShow = sender
        newMsg.receiver = receiverUser
        newMsg.subject = subject
        newMsg.messageText.setText(text)

        dataManager.commit(newMsg)
    }

    @Override
    void delete(String source, Message msg) {
        if (source == TX) {
            msg.getMeta().setIsSenderRec(false)
            msg.getMeta().setIsSenderDel(true)
        } else if (source == RX) {
            msg.getMeta().setIsReceiverRec(false)
            msg.getMeta().setIsReceiverDel(true)
        } else
            throw new RuntimeException("Unknown source: " + source)
        dataManager.commit(msg.getMeta())
        dataManager.commit(msg)
    }

    @Override
    void favorite(String source, Message msg) {
        //TODO favorites bean
    }

    @Override
    void recycle(String source, Message msg) {
        if (source == TX)
            msg.getMeta().setIsSenderRec(true)
        else if (source == RX)
            msg.getMeta().setIsReceiverRec(true)
        else
            throw new RuntimeException("Unknown source: " + source)
        dataManager.commit(msg.getMeta())
        dataManager.commit(msg)
    }

    @Override
    void recover(String source, Message msg) {
        if (source == TX)
            msg.getMeta().setIsSenderRec(false)
        else if (source == RX)
            msg.getMeta().setIsReceiverRec(false)
        else
            throw new RuntimeException("Unknown source: " + source)
        dataManager.commit(msg.getMeta())
        dataManager.commit(msg)
    }
}